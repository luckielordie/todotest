from conans import ConanFile, CMake, tools


class TodoConan(ConanFile):
    name = "Todo"
    version = "0.1.0"
    license = "Free"
    url = "http://some.url.com"
    description = "<Description of Todo here>"
    settings = "os", "compiler", "build_type", "arch", "os_build", "arch_build"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake_multi"
    exports_sources = "*"
    requires = (("gtest/1.8.0@bincrafters/stable"))

    def build(self):
        tools.mkdir("build")
        cmake = CMake(self)
        cmake.configure(build_folder="build")
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="source")
        self.copy("*toDo.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["toDo"]
