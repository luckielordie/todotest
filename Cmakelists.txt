cmake_minimum_required(VERSION 2.8 FATAL_ERROR)
project("Todo")

include(conanbuildinfo_multi.cmake)

conan_basic_setup(TARGETS)

add_library(toDo source/todo.cpp)